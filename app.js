const express = require('express')
const bodyParser = require('body-parser')


const app = express()

app.use(bodyParser.json())
let db = [
    {
        id:1,
        name: 'Haekal',
        address: 'Medan',
        email: 'hkl@gmail.com'
    },
    {
        id:2,
        name: 'mora',
        address: 'Depok',
        email: 'mrhakim@gmail.com'
    },
    {
        id:3,
        name: 'khuzain',
        address: 'banjarmasin',
        email: 'zain@gmail.com'
    }
]
let mentor = [
    {
        id: 1,
        name : 'zaki arsyad',
        address: 'jakarta'
    },
    {
        id:2,
        name: 'ahmed',
        address: 'surabaya'
    }
]

app.get('/', (req, res) => res.send('Batch 3 Synrgy'))

app.get('/student', (req,res) => {
    return res.json(siswa);
} )

app.get('/teacher', (req,res) => {
    return res.json(mentor);
})

app.get('/student/:name', (req, res) => {
    const result = db.filter(hsl => {
        return hsl.name.toLocaleLowerCase() === req.params.name.toLocaleLowerCase();
    })
    return res.json(result);
} )
app.get('/teacher/:name', (req, res) => {
    const hasil = mentor.filter(hsl => {
        return hsl.name.toLocaleLowerCase() === req.params.name.toLocaleLowerCase();
    })
    return res.json(hasil);
} )


app.listen(3000, () => {
    console.log(`Example app listening at http://localhost:3000`);
  })